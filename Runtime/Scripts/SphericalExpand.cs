using UnityEngine;

namespace Walid.ThreeDimension
{
    public class SphericalExpand : MonoBehaviour
    {
        #region Public Properties
        public float expandRatio, sphereRadius;
        public Transform Parent;
        public Transform[] childTransforms;
        #endregion Public Properties

        #region Private Properties
        private int totalChilds;
        private Vector3[] childInitialPos, childTargetPos;
        #endregion Private Properties

        #region Public Methods
        /// <summary>
        /// Initialize Object to expanding
        /// </summary>
        /// <param name="parent">The parent transfor we want to expand</param>
        public void Initialized(Transform parent)
        {
            Parent = parent;
            sphereRadius = 0f;
            MeshFilter[] meshFilters = parent.GetComponentsInChildren<MeshFilter>(includeInactive: true);
            totalChilds = meshFilters.Length;
            childTransforms = new Transform[totalChilds];
            childInitialPos = new Vector3[totalChilds];
            childTargetPos = new Vector3[totalChilds];
            for (int i = 0; i < totalChilds; i++)
            {
                childTransforms[i] = meshFilters[i].transform;
                childInitialPos[i] = childTransforms[i].position;
                float distanceFromParentObj = Vector3.Distance(a: Parent.position, b: childInitialPos[i]);
                sphereRadius = sphereRadius > distanceFromParentObj ? sphereRadius : distanceFromParentObj * expandRatio;
            }
            for (int i = 0; i < totalChilds; i++)
                childTargetPos[i] = NearestIntersectPoint(center: Parent.position, point: childInitialPos[i], radius: sphereRadius);
        }

        /// <summary>
        /// Spherical Expanding with Unity UI slider
        /// </summary>
        /// <param name="value">Slider value between 0 to 1</param>
        public void Expanding(float value)
        {
            for (int i = 0; i < totalChilds; i++)
                childTransforms[i].position = Vector3.Lerp(a: childInitialPos[i], b: childTargetPos[i], t: value);
        }

        /// <summary>
        /// To Find the Intersection Point of a Line and Sphere
        /// Where the Line's two points are the Center & Point and
        /// The Line is a Diameter of the Sphere
        /// <see href="https://gitlab.com/UnityPackage/SphericalExpanding/-/blob/main/Sprites/IntersectionOfLine&Sphere.svg">Formula</see>
        /// </summary>
        /// <param name="center">Sphere Center</param>
        /// <param name="point">Second Point of Line</param>
        /// <param name="radius">The Radius of the Sphere</param>
        public Vector3 NearestIntersectPoint(Vector3 center, Vector3 point, float radius)
        {
            if (center.Equals(point))
                return center + Random.onUnitSphere * radius;
            float deltaX = center.x - point.x, deltaY = center.y - point.y, deltaZ = center.z - point.z, constant = radius / Mathf.Sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);  //Calculate Constant
            Vector3 intersectPointA = new Vector3(center.x + deltaX * constant, center.y + deltaY * constant, center.z + deltaZ * constant), intersectPointB = new Vector3(center.x - deltaX * constant, center.y - deltaY * constant, center.z - deltaZ * constant);
            return Vector3.Distance(a: point, b: intersectPointA) < Vector3.Distance(a: point, b: intersectPointB) ? intersectPointA : intersectPointB;
        }
        #endregion Public Methods
    }
}
